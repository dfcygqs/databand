import request from '@/utils/request'
import { getToken } from '@/utils/auth'

// 查询字典数据列表
export function listData(query) {
  return request({
    url: '/system/dict/data/list',
    method: 'get',
    params: query
  })
}

// 查询字典数据详细
export function getData(dictCode) {
  return request({
    url: '/system/dict/data/' + dictCode,
    method: 'get'
  })
}

// 根据字典类型查询字典数据信息
export function getDicts(dictType) {
  return request({
    url: '/system/dict/data/type/' + dictType,
    method: 'get'
  })
}


export function getDictsSync(dictType) {
  var oReq = new XMLHttpRequest();
  oReq.open("GET", process.env.VUE_APP_BASE_API + "/system/dict/data/type/"+ dictType, false); // 同步请求
  oReq.setRequestHeader("Content-type", "application/json");
  oReq.setRequestHeader("Authorization", 'Bearer ' + getToken());
  //oReq.timeout = 2000; // 超时时间，单位是毫秒
  //oReq.send(JSON.stringify({"name": value}));//发送数据需要自定义，这里发送的是JSON结构
  oReq.send();

  var result = oReq.responseText;//响应结果
  return result;
}

// 新增字典数据
export function addData(data) {
  return request({
    url: '/system/dict/data',
    method: 'post',
    data: data
  })
}

// 修改字典数据
export function updateData(data) {
  return request({
    url: '/system/dict/data',
    method: 'put',
    data: data
  })
}

// 删除字典数据
export function delData(dictCode) {
  return request({
    url: '/system/dict/data/' + dictCode,
    method: 'delete'
  })
}

// 导出字典数据
export function exportData(query) {
  return request({
    url: '/system/dict/data/export',
    method: 'get',
    params: query
  })
}