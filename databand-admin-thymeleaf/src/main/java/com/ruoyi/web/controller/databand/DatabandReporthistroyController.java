package com.ruoyi.web.controller.databand;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.web.domain.DatabandReporthistroy;
import com.ruoyi.web.service.IDatabandReporthistroyService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 报版本Controller
 * 
 * @author databand
 * @date 2020-12-31
 */
@Controller
@RequestMapping("/web/reporthistroy")
public class DatabandReporthistroyController extends BaseController
{
    private String prefix = "web/reporthistroy";

    @Autowired
    private IDatabandReporthistroyService databandReporthistroyService;

    @RequiresPermissions("web:reporthistroy:view")
    @GetMapping()
    public String reporthistroy()
    {
        return prefix + "/reporthistroy";
    }

    /**
     * 查询报版本列表
     */
    @RequiresPermissions("web:reporthistroy:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(DatabandReporthistroy databandReporthistroy)
    {
        startPage();
        List<DatabandReporthistroy> list = databandReporthistroyService.selectDatabandReporthistroyList(databandReporthistroy);
        return getDataTable(list);
    }

    /**
     * 导出报版本列表
     */
    @RequiresPermissions("web:reporthistroy:export")
    @Log(title = "报版本", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(DatabandReporthistroy databandReporthistroy)
    {
        List<DatabandReporthistroy> list = databandReporthistroyService.selectDatabandReporthistroyList(databandReporthistroy);
        ExcelUtil<DatabandReporthistroy> util = new ExcelUtil<DatabandReporthistroy>(DatabandReporthistroy.class);
        return util.exportExcel(list, "reporthistroy");
    }

    /**
     * 新增报版本
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存报版本
     */
    @RequiresPermissions("web:reporthistroy:add")
    @Log(title = "报版本", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(DatabandReporthistroy databandReporthistroy)
    {
        return toAjax(databandReporthistroyService.insertDatabandReporthistroy(databandReporthistroy));
    }

    /**
     * 修改报版本
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        DatabandReporthistroy databandReporthistroy = databandReporthistroyService.selectDatabandReporthistroyById(id);
        mmap.put("databandReporthistroy", databandReporthistroy);
        return prefix + "/edit";
    }

    /**
     * 修改保存报版本
     */
    @RequiresPermissions("web:reporthistroy:edit")
    @Log(title = "报版本", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(DatabandReporthistroy databandReporthistroy)
    {
        return toAjax(databandReporthistroyService.updateDatabandReporthistroy(databandReporthistroy));
    }

    /**
     * 删除报版本
     */
    @RequiresPermissions("web:reporthistroy:remove")
    @Log(title = "报版本", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(databandReporthistroyService.deleteDatabandReporthistroyByIds(ids));
    }
}
