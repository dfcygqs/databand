package com.ruoyi.web.controller.databand;
import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.web.domain.DatabandScheduletask;
import com.ruoyi.web.service.IDatabandScheduletaskService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 批处理计划Controller
 * 
 * @author databand
 * @date 2020-12-31
 */
@Controller
@RequestMapping("/web/scheduletask")
public class DatabandScheduletaskController extends BaseController
{
    private String prefix = "web/scheduletask";

    @Autowired
    private IDatabandScheduletaskService databandScheduletaskService;

    @RequiresPermissions("web:scheduletask:view")
    @GetMapping()
    public String scheduletask()
    {
        return prefix + "/scheduletask";
    }

    /**
     * 查询批处理计划列表
     */
    @RequiresPermissions("web:scheduletask:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(DatabandScheduletask databandScheduletask)
    {
        startPage();
        List<DatabandScheduletask> list = databandScheduletaskService.selectDatabandScheduletaskList(databandScheduletask);
        return getDataTable(list);
    }

    /**
     * 导出批处理计划列表
     */
    @RequiresPermissions("web:scheduletask:export")
    @Log(title = "批处理计划", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(DatabandScheduletask databandScheduletask)
    {
        List<DatabandScheduletask> list = databandScheduletaskService.selectDatabandScheduletaskList(databandScheduletask);
        ExcelUtil<DatabandScheduletask> util = new ExcelUtil<DatabandScheduletask>(DatabandScheduletask.class);
        return util.exportExcel(list, "scheduletask");
    }

    /**
     * 新增批处理计划
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存批处理计划
     */
    @RequiresPermissions("web:scheduletask:add")
    @Log(title = "批处理计划", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(DatabandScheduletask databandScheduletask)
    {
        return toAjax(databandScheduletaskService.insertDatabandScheduletask(databandScheduletask));
    }

    /**
     * 修改批处理计划
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        DatabandScheduletask databandScheduletask = databandScheduletaskService.selectDatabandScheduletaskById(id);
        mmap.put("databandScheduletask", databandScheduletask);
        return prefix + "/edit";
    }

    /**
     * 修改保存批处理计划
     */
    @RequiresPermissions("web:scheduletask:edit")
    @Log(title = "批处理计划", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(DatabandScheduletask databandScheduletask)
    {
        return toAjax(databandScheduletaskService.updateDatabandScheduletask(databandScheduletask));
    }

    /**
     * 删除批处理计划
     */
    @RequiresPermissions("web:scheduletask:remove")
    @Log(title = "批处理计划", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(databandScheduletaskService.deleteDatabandScheduletaskByIds(ids));
    }
}
