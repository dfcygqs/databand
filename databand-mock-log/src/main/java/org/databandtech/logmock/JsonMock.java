package org.databandtech.logmock;

import java.time.Instant;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import org.databandtech.common.JsonFileUtil;
import org.databandtech.common.Mock;
import org.json.JSONArray;
import org.json.JSONObject;


public class JsonMock {
	static int COUNT = 20;
	static int AGE_LOW = 20;
	static int AGE_HIGH = 100;
	static String FILE_PATH = "c:\\logs\\json\\";
	static String FILE_NAME = "newfile"+Instant.now().getEpochSecond();

	public static void main(String[] args) throws Exception {

		JSONArray jsonArr = new JSONArray();
		
		for(int i=0;i<COUNT;i++) {

	        Map<String, Object> map = new HashMap<>();
	        map.put("Name",Mock.getChineseName());
	        map.put("Address", Mock.getRoad());
	        map.put("Age", Mock.getNum(AGE_LOW,AGE_HIGH));
	        map.put("Phone", Mock.getTel());
	        map.put("Date", LocalDate.of(2020, Mock.getNum(1, 10), Mock.getNum(1, 30)).toString());
	        map.put("V1", Mock.getNum(100, 300));
	        jsonArr.put(map);
		}
				
		JSONObject jo = new JSONObject();
		jo.put("id", FILE_NAME);
		jo.put("name", "Demo");
		jo.put("ObjectArray", jsonArr);
	    String jsonString = jo.toString();
	    JsonFileUtil.createJsonFile(jsonString, FILE_PATH, FILE_NAME);
	    System.out.println(FILE_PATH + FILE_NAME + "数据导出成功");
	}

}
