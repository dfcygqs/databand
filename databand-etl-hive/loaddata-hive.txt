内部表（管理表）

分隔符
定义了三中分割，有字段分隔，集合项分隔，key-valur分隔
create table t1(
    id      int
   ,name    string
   ,hobby   array<string>
   ,add     map<String,string>
)
row format delimited
fields terminated by ','
collection items terminated by '-'
map keys terminated by ':';

分区表
CREATE TABLE page_view1(viewTime INT, userid BIGINT,
                page_url STRING, referrer_url STRING,
                ip STRING COMMENT 'IP Address of the User')
COMMENT 'This is the page view table'
PARTITIONED BY(dt STRING, country STRING)
STORED AS SEQUENCEFILE;

列出表的分区。如果表没有分区，则抛出错误。
SHOW PARTITIONS page_view1;
列出表的列和列的类型
DESCRIBE page_view;

使用insert语句“指定分区”插入
create table tb_insert_part(id int,name string)
partitioned by(sex string);

insert overwrite table tb_insert_part partition(sex = 'male')
from XX
select id,name from tb_select1 where sex='male';

动态分区插入
create table tb_dy_part(id int,name string) partitioned by(sex string);
from XX
insert overwrite table tb_dy_part partition(sex)

select id,name,sex from tb_select1;


装载本地数据
load data local inpath '/home/hive.txt' overwrite into table t1;

装载hdfs数据
load data inpath '/hive/test/load2.txt' into table tb_load1;

使用overwrite关键字覆盖数据
load data local inpath '/home/hadoop/load3.txt' overwrite into table tb_load1;

外部表
use hive_1;

hadoop fs -mkdir /user/data/staging/
hadoop dfs -put /home/pv20100726.txt /user/data/staging/page_view

CREATE EXTERNAL TABLE pve(viewTime INT, userid BIGINT, page_url STRING,referrer_url STRING,ip STRING COMMENT 'IP Address of the User',country STRING COMMENT 'country of origination') 
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','  
LINES TERMINATED BY '\n' 
STORED AS TEXTFILE 
LOCATION '/user/data/staging/page_view';


如果这个执行出现
There are 2 datanode(s) running and 2 node(s) are excluded in this operation.
那是因为防火墙的问题。
暂时关闭
systemctl stop firewalld

表传表
FROM page_view_stg pvs
INSERT OVERWRITE TABLE page_view PARTITION(dt='2016-06-08', country='US')
SELECT pvs.viewTime, pvs.userid, pvs.page_url, pvs.referrer_url, null, null, pvs.ip
WHERE pvs.country = 'US';

系统也支持直接从本地文件系统上加载数据到Hive表。表的格式与输入文件的格式需要相同。如果文件/tmp/pv_2016-06-08包含了US数据，然后我们不需要像前面例子那样的任何筛选，这种情况的加载可以使用以下语法完成：

CREATE TABLE pveLocal(viewTime INT, userid BIGINT, page_url STRING,referrer_url STRING,ip STRING COMMENT 'IP Address of the User',country STRING COMMENT 'country of origination') 
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','  
LINES TERMINATED BY '\n' 
STORED AS TEXTFILE 

CREATE TABLE pveLocalPART(viewTime INT, userid BIGINT, page_url STRING,referrer_url STRING,ip STRING COMMENT 'IP Address of the User') 
PARTITIONED BY(dt STRING, country STRING) 
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','  
LINES TERMINATED BY '\n' 
STORED AS TEXTFILE ;


LOAD DATA LOCAL INPATH '/home/pv20200726.txt' INTO TABLE pveLocal;
LOAD DATA LOCAL INPATH '/home/pv20200726.txt' INTO TABLE pveLocalPART PARTITION(dt='2016-06-08', country='US');

FROM pveLocal pvs
INSERT OVERWRITE TABLE pveLocalPART PARTITION(dt='2016-06-08', country='US')
SELECT pvs.viewTime, pvs.userid, pvs.page_url, pvs.referrer_url, pvs.ip
WHERE pvs.referrer_url = 'main';

select t.userid,t.page_url,t.ip from pve t;

数据导出到hdfs

from tb_select1 t
insert overwrite  directory '/hive/test/male'
select t.id,t.name,t.sex where t.sex='male'
insert overwrite  directory '/hive/test/female'
select t.id,t.name,t.sex where t.sex='female';

from pveLocal pvs
insert overwrite  directory '/hive/pveLocal'
SELECT pvs.viewTime, pvs.userid, pvs.page_url, pvs.referrer_url, pvs.ip;

从hive库导出文件到本地文件系统
insert overwrite local directory '/data8/demo' row format delimited fields terminated by '^'  select * from test_hive_table where op_month= 201701 and op_time= 20170111 and op_hour= 2017011111 ;

本章节仅限于ETL，不涉及hive查询，结束。