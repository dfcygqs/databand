import Layout from '@/layout'

const sitemenuRouter = {
  path: '/sitemenus',
  component: Layout,
  redirect: 'noRedirect',
  name: 'sitesmenu',
  meta: {
    title: '站点菜单',
    icon: 'chart'
  },
  children: [
    {
      path: 'line',
      component: () => import('@/views/charts/line'),
      name: 'LineChart',
      meta: { title: '静态LineChart', noCache: true }
    },
    {
      path: 'pie',
      component: () => import('@/views/charts/pie'),
      name: 'PieChart',
      meta: { title: '静态PieChart', noCache: true }
    },
    {
      path: 'radar',
      component: () => import('@/views/charts/radar'),
      name: 'radarChart',
      meta: { title: '静态RadarChart', noCache: true }
    },
    {
      path: 'dataset',
      component: () => import('@/views/charts/dataset'),
      name: 'DatasetChart',
      meta: { title: '静态DatasetChart', noCache: true }
    },
    {
      path: 'mix-chart',
      component: () => import('@/views/charts/mix-chart'),
      name: 'MixChart',
      meta: { title: '动态MixChart', noCache: true }
    },
    {
      path: 'dylinechart',
      component: () => import('@/views/charts/dyline'),
      name: 'dylinechart',
      meta: { title: '动态LineChart', noCache: true }
    },
    {
      path: 'dydatasetchart',
      component: () => import('@/views/charts/dydataset'),
      name: 'dydatasetchart',
      meta: { title: '动态DatasetChart', noCache: true }
    },
    {
      path: 'dyppiechart',
      component: () => import('@/views/charts/dypie'),
      name: 'dypiechart',
      meta: { title: '动态PieChart', noCache: true }
    },
    {
      path: 'dypradarchart',
      component: () => import('@/views/charts/dyradar'),
      name: 'dyradarchart',
      meta: { title: '动态RadarChart', noCache: true }
    }
  ]
}

export default sitemenuRouter
