package org.databandtech.streamjob.sink;

import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class SinkToMySQL extends RichSinkFunction<String> {

	Connection connection = null;
    PreparedStatement preparedStatement = null;
    String[] COLUMNS;
    Tuple item;
    String SQL;
	String URL = "";
    String USERNAME = "";
    String PASSWORD = "";
    public SinkToMySQL(String uRL, String uSERNAME, String pASSWORD,String sQL) {
		super();
		SQL = sQL;
		URL = uRL;
		USERNAME = uSERNAME;
		PASSWORD = pASSWORD;
	}

    @Override
    public void open(Configuration parameters) throws Exception {
        super.open( parameters );
        String driver = "com.mysql.jdbc.Driver";
        Class.forName( driver );
        connection = DriverManager.getConnection( URL, USERNAME, PASSWORD );
        preparedStatement = connection.prepareStatement(SQL);
    }

    @Override
    public void close() throws Exception {
        super.close();
        if (connection != null) {
            connection.close();
        }
        if (preparedStatement != null) {
            preparedStatement.close();
        }
    }

    @Override
    public void invoke(String str, Context context) throws Exception {
        try {
        	
        	String[]  arr = str.split(",");
        	
        	//System.out.println(tuple.f0);
        	if (arr[0]!=null)
            preparedStatement.setString( 1, arr[0] );
        	if (arr[1]!=null)
            preparedStatement.setInt( 2, Integer.parseInt(arr[1]) );
        	if (arr[2]!=null)
            preparedStatement.setBigDecimal( 3, new BigDecimal(arr[2]) );
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
